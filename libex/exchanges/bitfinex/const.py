
# Websocket服务地址
# WS_URL = 'wss://api-pub.bitfinex.com/ws/2'
MARKET_WS_URL = 'wss://api-pub.bitfinex.com/ws/2'
INDIVIDUAL_WS_URL = 'wss://api.bitfinex.com/ws/2'

INDIVIDUAL_REST_URL = 'https://api.bitfinex.com/'

# 支持的交易对
SYMBOLS = {
    'tBTCUSD':('btc','usd'),
    'tETHUSD':('eth','usd'),
    'tLTCUSD':('ltc','usd'),
    'tLEOUSD':('leo','usd'),
    'tBTCUST':('btc','ust'),
    'tRRBUST':('rrb','ust')
    }


